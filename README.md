# Private Docker Registry

[Caddy] is the HTTP/2 proxy with automatic HTTPS.

[Docker registry] is a private docker registry.

[Docker auth] is a authn/authz server for private docker registry.

[Caddy] + [Docker registry] + [Docker auth] = A secure private registry with authn/authz.

## Prerequisites


#### Add test site name to /etc/hosts or your DNS server

E.g. the test site FQDN is _docker-example.somdev.stanford.edu_

```
echo "127.0.0.1 docker-example.somdev.stanford.edu" >> /etc/hosts
```

#### Optional GCP Auth

If you want try out _Automatic HTTPS_ with Google Cloud DNS Challenge,
you need to have _gcloud_ installed. See [Tools We Use](https://code.stanford.edu/xuwang/docker-examples/blob/master/tools.md)

And you must have admin rights on a managed DNS Zone in your GCP Project defined in envs. To get the GCP credentials:

```
$ gcloud auth login
```

## Bring it up

```
$ make up
```

## Shut it down

```
$ make down
```

[Caddy]: https://caddyserver.com/
[Docker registry]: https://docs.docker.com/registry/#tldr
[Docker auth]: https://github.com/cesanta/docker_auth
